import argparse
import configparser
from pathlib import Path

from lib import get_connection

def main(argv=None):
    """
    Sender
    """
    parser = argparse.ArgumentParser(prog=__file__, description=main.__doc__)
    parser.add_argument('--config',
                        default=Path(__file__).parent / 'instance' / 'gfsweb01.ini',
                        help='Path to config file. Default: %(default)s')
    parser.add_argument('--count', type=int, default=1, metavar='N',
                        help='Send N times. Default: %(default)s')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.config)

    connection = get_connection(config)
    channel = connection.channel()
    queue_name = channel.queue_declare(queue='helloworld').method.queue
    print(f" Queue name: {queue_name}")

    for i in range(args.count):
        body = f'[{i}] Hello World!'
        channel.basic_publish(exchange = config['queue_bind']['exchange'],
                              routing_key = queue_name,
                              body = body)
        print(f" [x] Sent {body}")
    connection.close()

if __name__ == '__main__':
    main()
