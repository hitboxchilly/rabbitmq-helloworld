import pika

def get_connection(config):
    return pika.BlockingConnection(
        pika.ConnectionParameters(
            credentials = pika.PlainCredentials(
                **config['credentials']
            ),
            **config['connection_parameters']
        )
    )
