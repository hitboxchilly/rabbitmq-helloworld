import argparse
import configparser
from pathlib import Path

from lib import get_connection

def main(argv=None):
    """
    Receiver
    """
    parser = argparse.ArgumentParser(prog=__file__, description=main.__doc__)
    parser.add_argument('--config',
                        default=Path(__file__).parent / 'instance' / 'gfsweb01.ini',
                        help='Path to config file. Default: %(default)s')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.config)

    connection = get_connection(config)
    channel = connection.channel()
    queue_name = channel.queue_declare(queue='helloworld').method.queue
    print(f" Queue name: {queue_name}")

    channel.queue_bind(exchange = config['queue_bind']['exchange'], queue = queue_name)

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(callback, queue=queue_name, no_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
